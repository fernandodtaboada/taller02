#!/bin/bash
echo "Definiendo Variables"
KEYNAME=$1
SECNAME=$2
echo "creando llave"
aws ec2 create-key-pair --key-name $KEYNAME --query 'KeyMaterial' --output text > ${KEYNAME}.pem
echo "dando permisos a la llave"
chmod 600 ${KEYNAME}.pem
echo "creando security group"
aws ec2 create-security-group --group-name $SECNAME --description "web security group" > secgroup.json
echo "parseando id de security group"
SECID=$(cat secgroup.json| jq -r ".GroupId")
echo "abriendo puerto 22 de comunicacion SSH"
aws ec2 authorize-security-group-ingress --group-id $SECID --protocol tcp --port 22 --cidr 0.0.0.0/0
echo "creando instancia linux"
aws ec2 run-instances --image-id ami-0b59bfac6be064b78 --count 1 --instance-type t2.micro --key-name $KEYNAME --security-groups $SECNAME